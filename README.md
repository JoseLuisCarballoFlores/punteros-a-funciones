
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

int Suma(int x, int y){
	return x+y;
}

int Resta(int x, int y){
	return x-y;
}



int main(){

int (*pf[])(int,int)={Suma,Resta};

int opc;
int x,y;

do{

printf("Marca 1 para sumar\n");
printf("Marca 2 para restar\n");
scanf("%i",&opc);
}while(opc>2);

printf("Escribe el primer numero\n");
scanf("%i",&x);

printf("Escribe el segundo numero\n");
scanf("%i",&y);

printf("El resultado de la operacion es %i",(pf)[opc-1](x,y));


return 0;
}
	
